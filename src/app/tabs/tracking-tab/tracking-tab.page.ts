import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Observable, Subject } from 'rxjs';
import { TrackingService } from 'src/app/services/tracking.service';

@Component({
  selector: 'app-tracking-tab',
  templateUrl: 'tracking-tab.page.html',
  styleUrls: ['tracking-tab.page.scss'],
})
export class TrackingTabPage {
  elapsedTime$: Observable<string>;

  resetTimerRequest$ = new Subject<void>();

  constructor(public trackingService: TrackingService, public toastController: ToastController) {}

  resetTimer() {
    this.trackingService.resetTimer();
    this.resetTimerRequest$.next();
  }

  async save() {
    const toast = await this.toastController.create({
      message: 'comming soon!', duration: 1500
    });
    toast.present();
  }
}
