import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';
import { HistoryTabPage } from './history-tab.page';
import { HistoryTabPageRoutingModule } from './history-tab-routing.module';



@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    HistoryTabPageRoutingModule
  ],
  declarations: [HistoryTabPage]
})
export class HistoryTabPageModule {}
