import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tracking-tab',
        loadChildren: () => import('./tracking-tab/tracking-tab.module').then(m => m.TrackingTabPageModule)
      },
      {
        path: 'history-tab',
        loadChildren: () => import('./history-tab/history-tab.module').then(m => m.HistoryTabPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/tracking-tab',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tracking-tab',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
