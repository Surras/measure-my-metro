import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { TrackingService } from 'src/app/services/tracking.service';

@Component({
  selector: 'app-stopwatch',
  templateUrl: './stopwatch.component.html',
  styleUrls: ['./stopwatch.component.scss'],
})
export class StopwatchComponent implements OnDestroy {
  @Input()
  set resetRequest(obs: Observable<void>) {
    if (obs != null) {
      this.subscribtions.add(obs.pipe(tap(() => this.reset())).subscribe());
    }
  }

  subscribtions = new Subscription();

  timeBegan: Date = null;
  timeStopped: Date = null;
  timeElapsed: Date = null;
  stoppedDuration = 0;
  started = null;
  isRunning = false;
  blankTime = '00.00';
  time = '00.00';

  constructor(private trackingService: TrackingService) {}

  ngOnDestroy(): void {
    this.subscribtions.unsubscribe();
  }

  start() {
    if (this.isRunning) {
      return;
    }
    if (this.timeBegan === null) {
      this.reset();
      this.timeBegan = new Date();
    }
    if (this.timeStopped !== null) {
      const newStoppedDuration: number =
        +new Date() - this.timeStopped.valueOf();
      this.stoppedDuration = this.stoppedDuration + newStoppedDuration;
    }
    this.started = setInterval(this.clockRunning.bind(this), 20);
    this.isRunning = true;
  }

  stop() {
    this.isRunning = false;
    this.timeStopped = new Date();
    clearInterval(this.started);
    this.trackingService.setElapsedTime(this.timeElapsed);
  }

  reset() {
    this.isRunning = false;
    clearInterval(this.started);
    this.stoppedDuration = 0;
    this.timeBegan = null;
    this.timeStopped = null;
    this.time = this.blankTime;
  }

  clockRunning() {
    const currentTime: Date = new Date();
    this.timeElapsed = new Date(
      currentTime.valueOf() - this.timeBegan.valueOf() - this.stoppedDuration
    );

    const sec = this.timeElapsed.getUTCSeconds();
    const ms = this.timeElapsed.getUTCMilliseconds();

    this.time =
      sec.toString().padStart(2, '0') +
      '.' +
      ms.toString().padStart(2, '0').substring(0, 2);
  }
}
