import { SimplePlaceholderMapper } from '@angular/compiler/src/i18n/serializers/serializer';
import { AfterViewInit } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { TrackingService } from 'src/app/services/tracking.service';

interface TramType {
  type: 'flexityF8' | 'GT6';
  lengths: ReadonlyArray<number>;
  imageUrl: string;
  cardTitle: string;
}

@Component({
  selector: 'app-metro-chooser',
  templateUrl: './metro-chooser.component.html',
  styleUrls: ['./metro-chooser.component.scss'],
})
export class MetroChooserComponent implements AfterViewInit {
  @ViewChild('tramSlider') tramSlider;
  tramTypes: ReadonlyArray<TramType> = [
    {
      type: 'flexityF8',
      lengths: [30, 40],
      cardTitle: 'Flexity F8 / F8Z / F6Z',
      imageUrl: 'assets/img/flexity_tram.jpg',
    },
    {
      type: 'GT6',
      lengths: [27, 58],
      cardTitle: 'GT6 / GTZ / GTU',
      imageUrl: 'assets/img/gt6_gtz_gtu_tram.jpg',
    },
  ];
// test comment
  selectedTram$ = new Subject<TramType>();

  constructor(private trackingService: TrackingService) {}
  ngAfterViewInit() {
    const initialTram = this.tramTypes[0];
    this.selectedTram$.next(initialTram);
  }

  slideChanged() {
    this.tramSlider
      .getActiveIndex()
      .then((index) => this.selectedTram$.next(this.tramTypes[index]));
  }

  lengthSelectionChanged(event: any) {
    this.trackingService.setTramLength(event.detail.value);
  }
}
