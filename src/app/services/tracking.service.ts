import { Injectable } from '@angular/core';
import { Observable, Subject, combineLatest } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TrackingService {
  tramSpeed$: Observable<number>;
  private elapsedTime$ = new Subject<Date>();
  private tramLength$ = new Subject<number>();

  constructor() {
    this.tramSpeed$ = combineLatest([this.elapsedTime$, this.tramLength$])
      .pipe(
        map(([time, length]) => {
          const speed = (length / (time.getTime() / 1000)) * 3.6;
          return speed;
        })
      )
      .pipe(startWith(0));
  }

  setElapsedTime(time: Date) {
    this.elapsedTime$.next(time);
  }

  setTramLength(length: number) {
    this.tramLength$.next(length);
  }

  resetTimer() {
    this.elapsedTime$.next(new Date());
  }
}
